# Omeka Classic Docker Image

Omeka Classic is a web publishing platform for sharing digital collections and creating media-rich online exhibits.

https://omeka.org/classic/

## Links

* https://www.nginx.com/resources/wiki/start/topics/recipes/omeka/
* https://websiteforstudents.com/setup-omeka-classic-cms-on-ubuntu-16-04-17-10-18-04-with-nginx-mariadb-and-php-7-2-fpm/
