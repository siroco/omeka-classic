FROM registry.sindominio.net/debian as builder

# Install dependencies

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y git imagemagick wget unzip

RUN apt-get update && \
    apt-get -qy install \
    php-fpm php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip php-imagick\
    ghostscript wget sudo curl openssl imagemagick && \
    apt-get clean

RUN useradd omeka -d /omeka/

RUN mkdir /run/php/

RUN mkdir logs

# Install omeka

WORKDIR omeka

RUN chown omeka:omeka /omeka

USER omeka

RUN git clone https://github.com/omeka/Omeka.git
RUN chmod 775 Omeka

WORKDIR /omeka/Omeka

RUN touch .htaccess

USER root

RUN mkdir /var/log/php

RUN chown -R omeka:omeka /var/log/php

EXPOSE 9000

VOLUME /omeka/

CMD ["/usr/sbin/php-fpm7.4","--nodaemonize"]]
